<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<?php
function slick() {
    wp_register_script('slick', get_template_directory_uri() . '/js/slick.js', true );
    wp_enqueue_script('slick');
}

add_action( 'wp_enqueue_scripts', 'slick' );
?>
<?php
function custom() {
    wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', true );
    wp_enqueue_script('custom');
}

add_action( 'wp_enqueue_scripts', 'custom' );
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KWSGHDJ');</script>
<!-- End Google Tag Manager -->

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript" async defer src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KWSGHDJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="wrapper" class="hfeed">
<script>

</script>
<header id="header">

  <div id="contents">

    <div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg" alt="Carbon logo"></a></div>

    <nav id="menu">

      <div class="menu-top-navigation-container">

        <ul id="menu-top-navigation" class="menu">

        <?php if( have_rows('page_links') ): ?>

        	<?php while( have_rows('page_links') ): the_row();?>

            <?php
            $link = get_sub_field('link');

            if( $link ):
                $link_url = $link['url'];
                $link_title = $link['title'];
                $link_target = $link['target'] ? $link['target'] : '_self';
                $button = get_sub_field('button');
                ?>
                <li class="<?php if( get_sub_field('button') ): ?>wp-block-button is-style-green<?php endif; ?>"><a href="<?php echo esc_url( $link_url ); ?>" target="<?php echo esc_attr( $link_target ); ?>"><?php echo esc_html( $link_title ); ?></a></li>
            <?php endif; ?>

          	<?php endwhile; ?>

          <?php endif; ?>

        </ul>



        </div>

      </nav>

  </div>

</header>

<div id="circles">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
