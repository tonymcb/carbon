<?php
add_action( 'after_setup_theme', 'blankslate_setup' );
function blankslate_setup() {
load_theme_textdomain( 'blankslate', get_template_directory() . '/languages' );
add_theme_support( 'title-tag' );
add_theme_support( 'automatic-feed-links' );
add_theme_support( 'post-thumbnails' );
add_theme_support( 'html5', array( 'search-form' ) );
add_theme_support( 'align-wide' );
global $content_width;
if ( ! isset( $content_width ) ) { $content_width = 1920; }
register_nav_menus( array( 'main-menu' => esc_html__( 'Main Menu', 'blankslate' ) ) );
}
add_action( 'wp_enqueue_scripts', 'blankslate_load_scripts' );
function blankslate_load_scripts() {
wp_enqueue_style( 'blankslate-style', get_stylesheet_uri() );
wp_enqueue_script( 'jquery' );
}

function prefix_nav_description( $item_output, $item, $depth, $args ) {
    if ( !empty( $item->description ) ) {
        $item_output = str_replace( $args->link_after . '</a>', '<span class="menu-item-description">' . $item->description . '</span>' . $args->link_after . '</a>', $item_output );
    }

    return $item_output;
}

add_filter( 'walker_nav_menu_start_el', 'prefix_nav_description', 10, 4 );


add_filter( 'wp_list_categories', function( $html ) {
    return str_replace( ' current-cat', ' active', $html );
});

function be_gutenberg_scripts() {

	wp_enqueue_script(
		'be-editor',
		get_stylesheet_directory_uri() . '/js/editor.js',
		array( 'wp-blocks', 'wp-dom' ),
		filemtime( get_stylesheet_directory() . '/js/editor.js' ),
		true
	);
}

add_action( 'enqueue_block_editor_assets', 'be_gutenberg_scripts' );

// Custom blocks

add_action('acf/init', 'my_acf_init');
function my_acf_init() {

	// check function exists
	if( function_exists('acf_register_block') ) {

    // register a banner block
    acf_register_block(array(
      'name'				=> 'banner',
      'title'				=> __('Banner'),
      'description'		=> __('A custom banner block.'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'admin-comments',
      'mode' => 'edit',
      'keywords'			=> array( 'banner', 'header' ),
    ));

    // register timeline block
    acf_register_block(array(
      'name'				=> 'timeline',
      'title'				=> __('Timeline'),
      'description'		=> __('A timeline block'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'timeline' ),
    ));

    // register a about stats block
    acf_register_block(array(
      'name'				=> 'about-stats',
      'title'				=> __('About stats'),
      'description'		=> __('About stats block.'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'admin-comments',
      'mode' => 'edit',
      'keywords'			=> array( 'testimonial', 'quote' ),
    ));

    // register a stats block
    acf_register_block(array(
      'name'				=> 'stats',
      'title'				=> __('Stats'),
      'description'		=> __('General stats block.'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'admin-comments',
      'mode' => 'edit',
      'keywords'			=> array( 'stats' ),
    ));

    // register a features grid
    acf_register_block(array(
      'name'				=> 'features-grid',
      'title'				=> __('Features grid'),
      'description'		=> __('Features in a grid layout.'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'anchor' => 'true',
      'supports' => ['anchor' => true],
      'keywords'			=> array( 'feature grid' ),
    ));

    // register a products grid
    acf_register_block(array(
      'name'				=> 'products-grid',
      'title'				=> __('Products grid'),
      'description'		=> __('Products in a grid layout.'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'products grid', 'products' ),
    ));

    // register team members
    acf_register_block(array(
      'name'				=> 'team-members',
      'title'				=> __('Team members'),
      'description'		=> __('Team members'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'admin-users',
      'mode' => 'edit',
      'keywords'			=> array( 'team members', 'team' ),
    ));

    // register contact info
    acf_register_block(array(
      'name'				=> 'contact-info',
      'title'				=> __('Contact info'),
      'description'		=> __('Contact information'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'contact info', 'contact' ),
    ));

    // register calculator
    acf_register_block(array(
      'name'				=> 'lander-info',
      'title'				=> __('Lander info'),
      'description'		=> __('Lander information'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'lander info', 'lander', 'quote', 'calculator' ),
    ));

    // register small call to action
    acf_register_block(array(
      'name'				=> 'call-to-action-small',
      'title'				=> __('Call to action small'),
      'description'		=> __('Small call to action area'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'call to action', 'cta' ),
    ));

    // register large call to action
    acf_register_block(array(
      'name'				=> 'call-to-action-large',
      'title'				=> __('Call to action large'),
      'description'		=> __('Large call to action area'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'large call to action', 'cta', 'call to action' ),
    ));

    // register logos block
    acf_register_block(array(
      'name'				=> 'logos',
      'title'				=> __('Logos'),
      'description'		=> __('A simple logos block'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'logos', 'partners' ),
    ));

    // register logos block
    acf_register_block(array(
      'name'				=> 'resources-carousel',
      'title'				=> __('Resources Carousel'),
      'description'		=> __('A carousel of resource articles'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'resources', 'carousel' ),
    ));

    // register lander header block
    acf_register_block(array(
      'name'				=> 'lander-header',
      'title'				=> __('Landing page header'),
      'description'		=> __('Landing page header block'),
      'render_callback'	=> 'my_acf_block_render_callback',
      'category'			=> 'formatting',
      'icon'				=> 'forms',
      'mode' => 'edit',
      'keywords'			=> array( 'lander', 'SoapHeader' ),
    ));

	}
}

function my_acf_block_render_callback( $block ) {

	$slug = str_replace('acf/', '', $block['name']);

	if( file_exists( get_theme_file_path("/blocks/{$slug}.php") ) ) {
		include( get_theme_file_path("/blocks/{$slug}.php") );
	}

}


// Job vacancies

if( function_exists('acf_add_options_page') ) {

  acf_add_options_page(array(
		'page_title' 	=> 'Job vacancies',
		'menu_title'	=> 'Job vacancies',
		'menu_slug' 	=> 'job-vacancies',
		'capability'	=> 'edit_posts',
		'redirect'		=> false
	));

}


// Add post category to body class

add_filter('body_class','add_category_to_single');
function add_category_to_single($classes) {
  if (is_single() ) {
    global $post;
    foreach((get_the_category($post->ID)) as $category) {
      // add category slug to the $classes array
      $classes[] = $category->category_nicename;
    }
  }
  // return the $classes array
  return $classes;
}


add_action( 'wp_footer', 'blankslate_footer_scripts' );
function blankslate_footer_scripts() {
?>
<script>
jQuery(document).ready(function ($) {
var deviceAgent = navigator.userAgent.toLowerCase();
if (deviceAgent.match(/(iphone|ipod|ipad)/)) {
$("html").addClass("ios");
$("html").addClass("mobile");
}
if (navigator.userAgent.search("MSIE") >= 0) {
$("html").addClass("ie");
}
else if (navigator.userAgent.search("Chrome") >= 0) {
$("html").addClass("chrome");
}
else if (navigator.userAgent.search("Firefox") >= 0) {
$("html").addClass("firefox");
}
else if (navigator.userAgent.search("Safari") >= 0 && navigator.userAgent.search("Chrome") < 0) {
$("html").addClass("safari");
}
else if (navigator.userAgent.search("Opera") >= 0) {
$("html").addClass("opera");
}
});
</script>
<?php
}
add_filter( 'document_title_separator', 'blankslate_document_title_separator' );
function blankslate_document_title_separator( $sep ) {
$sep = '|';
return $sep;
}
add_filter( 'the_title', 'blankslate_title' );
function blankslate_title( $title ) {
if ( $title == '' ) {
return '...';
} else {
return $title;
}
}
add_filter( 'the_content_more_link', 'blankslate_read_more_link' );
function blankslate_read_more_link() {
if ( ! is_admin() ) {
return ' <a href="' . esc_url( get_permalink() ) . '" class="more-link">...</a>';
}
}
add_filter( 'excerpt_more', 'blankslate_excerpt_read_more_link' );
function blankslate_excerpt_read_more_link( $more ) {
if ( ! is_admin() ) {
global $post;
return ' <a href="' . esc_url( get_permalink( $post->ID ) ) . '" class="more-link">...</a>';
}
}
add_filter( 'intermediate_image_sizes_advanced', 'blankslate_image_insert_override' );
function blankslate_image_insert_override( $sizes ) {
unset( $sizes['medium_large'] );
return $sizes;
}
add_action( 'widgets_init', 'blankslate_widgets_init' );
function blankslate_widgets_init() {
register_sidebar( array(
'name' => esc_html__( 'Sidebar Widget Area', 'blankslate' ),
'id' => 'primary-widget-area',
'before_widget' => '<li id="%1$s" class="widget-container %2$s">',
'after_widget' => '</li>',
'before_title' => '<h3 class="widget-title">',
'after_title' => '</h3>',
) );
}
add_action( 'wp_head', 'blankslate_pingback_header' );
function blankslate_pingback_header() {
if ( is_singular() && pings_open() ) {
printf( '<link rel="pingback" href="%s" />' . "\n", esc_url( get_bloginfo( 'pingback_url' ) ) );
}
}
/**
 * Auto format a text field with number formating.
 * Apply the class "wpf-number-format" to the field to enable.
 *
 */
function wpf_number_format() {
	?>

<script type="text/javascript">
    
	console.log("WP Form Hacking");

	jQuery(function ($) {

		$('.wpforms-field-number-slider-hint').text(wpforms.numberFormat($('.wpforms-field-number-slider-hint').text()));

		$(document).on('change input', '.wpforms-field-number-slider input[type=range]', function (event) {
			var hintEl = $(event.target).siblings('.wpforms-field-number-slider-hint');
			hintEl.text(wpforms.numberFormat(hintEl.text()));
		});
		$('#wpforms-form-8292').on('wpformsAjaxSubmitSuccess', function (event) {
			console.log("Calculation complete");
			
			var data = $(this).serializeArray();
			var uniques = data[1].value;
			var category = data[0].value;
			var geo = data[2].value;
			var ecpm = 0.1;
			
			if (category == 0) {
				ecpm = 0.15;
			} else if (category == 1) {
				ecpm = 0.15;
			} else if (category == 2) {
				ecpm = 0.15;
			} else if (category == 3) {
				ecpm = 0.45;
			} else if (category == 4) {
				ecpm = 0.35;
			} else if (category == 5) {
				ecpm = 0.15;
			}
			
			var estimate = (uniques*ecpm/1000).toLocaleString();
			
			var t = setInterval(function () {
			  if ($("#calculationResult").length > 0) {
				clearInterval(t);
				$("#calculationResult").text("$" + estimate);
			  }
			}, 50);
			
		});
	});

</script>

	<?php
}
add_action( 'wpforms_wp_footer', 'wpf_number_format' );
/**
 * Show values in Dropdown, checkboxes and Multiple Choice.
 *
 * @link https://wpforms.com/developers/add-field-values-for-dropdown-checkboxes-and-multiple-choice-fields/
 *
 */
  
add_action( 'wpforms_fields_show_options_setting', '__return_true' );
add_action( 'comment_form_before', 'blankslate_enqueue_comment_reply_script' );
function blankslate_enqueue_comment_reply_script() {
if ( get_option( 'thread_comments' ) ) {
wp_enqueue_script( 'comment-reply' );
}
}
function blankslate_custom_pings( $comment ) {
?>
<li <?php comment_class(); ?> id="li-comment-<?php comment_ID(); ?>"><?php echo comment_author_link(); ?></li>
<?php
}
add_filter( 'get_comments_number', 'blankslate_comment_count', 0 );
function blankslate_comment_count( $count ) {
if ( ! is_admin() ) {
global $id;
$get_comments = get_comments( 'status=approve&post_id=' . $id );
$comments_by_type = separate_comments( $get_comments );
return count( $comments_by_type['comment'] );
} else {
return $count;
}
}
