<?php

/*
* Template Name: Calculator
* Template Post Type: page
*/

get_header();


?>

<div id="container">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>

<?php the_content(); ?>

<?php echo do_shortcode( '[wpforms id="8292" title="false" description="false"]' ); ?>

<?php endwhile; endif; ?>

<?php get_footer();?>
