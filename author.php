<?php get_header(); ?>

<div id="container">

<header class="header">
<div class="archive-meta"><?php if ( '' != the_archive_description() ) { echo esc_html( the_archive_description() ); } ?></div>
</header>

<div id="banner">
  <div id="banner-text">
    <h1 class="entry-title">Resources</h1>
    <h2>Looking for information to find out what’s new at Carbon? Or looking for tips on how to make more revenue from your audience data strategy? We have a range articles, ebooks and more to guide you through the world of audience data management.</h2>
  </div>
</div>

<?php
// the query
$wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'category__not_in' => array(12),'posts_per_page'=>-1)); ?>

<?php if ( $wpb_all_query->have_posts() ) : ?>

<ul id="posts" style="margin-top:50px;">

  <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>

    <li>
      <div>
        <header>
            <div class="tags"><?php the_tags( ' ', '' ); ?></div>
            <?php if ( has_post_thumbnail() ) : ?>
            <a href="<?php the_permalink(); ?>" class="link"><?php the_post_thumbnail( array(400, 300) ); ?></a>
            <?php endif; ?>
        </header>

        <div class="post-content">
          <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
          <div class="author">
            <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
            <?php the_author_posts_link(); ?>
          </div>
          <div class="meta">
            <span><?php the_date(); ?></span>
          </div>
          <div class="entry">
             <?php the_excerpt(); ?>
           </div>
        </div>

        <!-- <?php the_category( ', ' ); ?> -->

        <footer>
          <span class="wp-block-button is-style-arrow link is-style-arrow-link"><a href="<?php the_permalink(); ?>" class="wp-block-button__link">Read more</a></span>
        </footer>
      </div>

    </li>

  <?php endwhile; ?>

</ul>

<?php wp_reset_postdata(); ?>

<?php else : ?>
    <p><?php _e( 'Sorry, no posts matched your criteria.' ); ?></p>
<?php endif; ?>


<?php get_footer(); ?>
