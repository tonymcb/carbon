<?php get_header(); ?>

<div id="container">

  <header class="header">
  <div class="archive-meta"><?php if ( '' != the_archive_description() ) { echo esc_html( the_archive_description() ); } ?></div>
  </header>

  <div id="banner">
    <div id="banner-text">
      <h1 class="entry-title">Whoops!</h1>
      <h2>Page not found!</h2>
    </div>
  </div>

</header>

<div style="text-align:center;padding:50px 0;">

  <p>Sorry, we can't find that page! It might be an old link or maybe it moved : (</p>

  <div class="wp-block-button" style="margin-top:50px;">
    <a class="wp-block-button__link" href="<?php echo site_url(); ?>">Head on home</a>
  </div>

</div>

</div>
<?php get_footer(); ?>
