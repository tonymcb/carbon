<?php get_header(); ?>

<div id="container">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<div id="banner">
  <div id="banner-text">
    <h1 class="entry-title"><?php the_title(); ?></h1>
  </div>
  <div class="author">
    <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
    <span>
      <?php the_author_posts_link(); ?>
      <time datetime="<?php echo get_the_date('c'); ?>" itemprop="datePublished"><?php echo get_the_date(); ?></time>
    </span>
  </div>
</div>


<?php if( get_field('feature_image') == 'default' ): ?>
  <div id="featured-image">
    <?php the_post_thumbnail(); ?>
  </div>
<?php endif; ?>

<?php if( get_field('feature_image') == 'remove' ): ?>

<?php endif; ?>

<?php

$image = get_field('alternative_feature_image');

if( get_field('feature_image') == 'alternative' ): ?>
  <div id="featured-image">
    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
  </div>
<?php endif; ?>

<div id="article-content">

<?php the_content(); ?>

</div>

<div id="article-info">

  <div>

    <div class="tags">
      <?php the_tags( ' ', '' ); ?>
    </div>

    <div class="author">
      <?php echo get_avatar( get_the_author_meta( 'ID' )); ?>
      <?php the_author_posts_link(); ?>
    </div>

  </div>

</div>

<div id="related-posts">

  <ul id="posts" class="related">

  <?php
  //for use in the loop, list 5 post titles related to first tag on current post
  $tags = wp_get_post_tags($post->ID);
  if ($tags) {
  $first_tag = $tags[0]->term_id;
  $args=array(
  'tag__in' => array($first_tag),
  'post__not_in' => array($post->ID),
  'posts_per_page'=>3,
  'ignore_sticky_posts'=>1
  );

  $my_query = new WP_Query($args);
  if( $my_query->have_posts() ) {
  while ($my_query->have_posts()) : $my_query->the_post();

  ?>

  <li>
    <div>

      <header>
          <div class="tags"><?php the_tags( ' ' ); ?></div>
          <?php if ( has_post_thumbnail() ) : ?>
          <a href="<?php the_permalink(); ?>" class="link"><?php the_post_thumbnail( array(400, 300) ); ?></a>
          <?php endif; ?>
      </header>

      <div class="post-content">
        <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
        <div class="author">
          <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
          <?php the_author_posts_link(); ?>
        </div>
        <div class="entry">
           <?php the_excerpt(); ?>
         </div>
      </div>

      <!-- <?php the_category( ', ' ); ?> -->

      <footer>
        <span class="wp-block-button is-style-arrow link is-style-arrow-link"><a href="<?php the_permalink(); ?>" class="wp-block-button__link">Read more</a></span>
      </footer>

    </div>

  </li>


  <?php
  endwhile;
  }
  wp_reset_query();
  }
  ?>

  </ul>

</div>

<div id="comments">

<h2>Comments</h2>

<?php if ( comments_open() && ! post_password_required() ) { comments_template( '', true ); } ?>

<?php endwhile; endif; ?>

</div>

<?php get_footer(); ?>
