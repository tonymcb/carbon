<?php
/**
 * Block Name: Small call to action
 *
 * This is the template that displays the small call to action block.
 */

// get image field (array)
$avatar = get_field('avatar');

// create id attribute for specific styling
$id = 'cta-small-' . $block['id'];

?>

<div id="<?php echo $id; ?>" class="cta-small">

  <div>

    <h3><?php the_field('title'); ?></h3>
    <div class="content">
      <span><?php the_field('text'); ?></span>
      <div class="wp-block-button is-style-green"><a class="wp-block-button__link" href="<?php echo get_site_url(); ?>/contact-us">Book a demo</a></div>
    </div>

  </div>

</div>
