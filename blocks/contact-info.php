<?php
/**
 * Block Name: Contact info
 *
 * This is the template that displays the contact information block.
 */

// get image field (array)
$avatar = get_field('avatar');

?>


<div id="contact-info">

	<div id="details">

		<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>

		<a href="tel:<?php the_field('phone'); ?>"><?php the_field('phone'); ?></a>

		<span id="address"><?php the_field('address'); ?></span>

		<span><?php if( have_rows('social_links') ): ?></span>

	</div>

	<ul id="social-links">

	<?php while( have_rows('social_links') ): the_row();

		// vars
		$icon = get_sub_field('icon');
		$name = get_sub_field('name');
		$link = get_sub_field('link');

		?>

		<li>
			<a href="<?php echo $link; ?>">
	    	<span><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" /></span>
			</a>
		</li>

	<?php endwhile; ?>

	</ul>

	<?php endif; ?>

</div>
