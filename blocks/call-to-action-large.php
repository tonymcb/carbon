<?php
/**
 * Block Name: Large call to action
 *
 * This is the template that displays the large call to action block.
 */

// get image field (array)
$avatar = get_field('avatar');

// create id attribute for specific styling
$id = 'cta-large-' . $block['id'];

?>

<div id="<?php echo $id; ?>" class="cta-large">

  <div class="cta-text">

    <h3><?php the_field('title'); ?></h3>
    <div class="content">
      <span><?php the_field('text'); ?></span>
      <div class="wp-block-button is-style-green"><a class="wp-block-button__link" href="<?php echo get_site_url(); ?>/contact-us">Book a demo</a></div>
    </div>

  </div>

  <?php

  $image = get_field('image');

  if( !empty($image) ): ?>

  <div class="cta-image">
    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>"/>
  </div>

  <?php endif; ?>

</div>
