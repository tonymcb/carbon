<?php
/**
 * Block Name: Lander info
 *
 * This is the template that displays the lander information block.
 */

// get image field (array)
$avatar = get_field('avatar');

?>


<div id="lander-info">

	<div id="details">

		<a href="mailto:<?php the_field('email'); ?>"><?php the_field('email'); ?></a>

		<p id="message"><?php the_field('message'); ?></p>

	</div>

</div>
