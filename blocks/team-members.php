<?php
/**
 * Block Name: Team members
 *
 * This is the template that displays the team members block.
 */

// get image field (array)
$avatar = get_field('avatar');

// create id attribute for specific styling
$id = 'team-' . $block['id'];

?>


<?php if( have_rows('team_members') ): ?>

	<div class="team">

    <ul>

  	<?php while( have_rows('team_members') ): the_row();

  		// vars
  		$image = get_sub_field('image');
  		$name = get_sub_field('name');
  		$position = get_sub_field('position');

  		?>

  		<li class="team-member">
        <div class="photo"><img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" /></div>
        <strong><?php echo $name; ?></strong>
        <span class="capitalise"><?php echo $position; ?></span>
  		</li>

  	<?php endwhile; ?>

    </ul>

  </div>

<?php endif; ?>
