<?php
/**
 * Block Name: Products grid
 *
 * This is the template that displays the products grid block.
 */

 // get image field (array)
 $avatar = get_field('avatar');

 // create id attribute for specific styling
 $id = 'products-grid-' . $block['id'];

 ?>

 <?php if( have_rows('products_grid') ):

 	while( have_rows('products_grid') ): the_row();

 		// vars
 		$content = get_sub_field('content');

 		?>
 		<div id="<?php echo $id; ?>" class="products-grid bg">

      <div>

   			<?php the_sub_field('content'); ?>

        <ul>

          <?php while( have_rows('product_items') ): the_row();

            // vars
            $image = get_sub_field('icon');
            $description = get_sub_field('description');
            $title = get_sub_field('title');
            $pagelink = get_sub_field('page_link');

            ?>


              <li class="item">
                <div>
                  <div class="icon">
                    <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
                  </div>
                  <div class="content">
                    <h4><?php echo $title; ?></h4>
                    <p><?php echo $description; ?></p>
                    <div class="wp-block-button text is-style-arrow link is-style-arrow-link">
                      <a class="wp-block-button__link" href="<?php echo $pagelink; ?>">Find out more</a>
                    </div>
                  </div>
                </div>
              </li>


        	<?php endwhile; ?>

        </ul>

      </div>

 		</div>

 		<style type="text/css">
 			.bg {
 				background: <?php the_sub_field('background_colour'); ?>;
 			}
 		</style>

 	<?php endwhile; ?>

 <?php endif; ?>


 <?php

 // check if the flexible content field has rows of data
 if( have_rows('flexible_content_field_name') ):

  	// loop through the rows of data
     while ( have_rows('flexible_content_field_name') ) : the_row();

 		// check current row layout
         if( get_row_layout() == 'gallery' ):

         	// check if the nested repeater field has rows of data
         	if( have_rows('images') ):

 			 	echo '<ul>';

 			 	// loop through the rows of data
 			    while ( have_rows('images') ) : the_row();

 					$image = get_sub_field('image');

 					echo '<li><img src="' . $image['url'] . '" alt="' . $image['alt'] . '" /></li>';

 				endwhile;

 				echo '</ul>';

 			endif;

         endif;

     endwhile;

 else :

     // no layouts found

 endif;

 ?>
