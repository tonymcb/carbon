<?php
/**
 * Block Name: Logos
 *
 * This is the template that displays the small call to action block.
 */

// get image field (array)
$avatar = get_field('avatar');

// create id attribute for specific styling
$id = 'logos-' . $block['id'];

?>


<?php if( have_rows('logos') ): ?>

  <div id="<?php echo $id; ?>" class="logos">

    <ul>

  	<?php while( have_rows('logos') ): the_row();

  		// vars
  		$image = get_sub_field('image');

  		?>
  		<li>
        <img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt'] ?>" />
  		</li>

  	<?php endwhile; ?>

    </ul>

  </div>

<?php endif; ?>
