<?php
/**
 * Block Name: Lander header
 *
 * This displays the Lander header block.
 */

 ?>


 <div id="banner">

   <div>

   	<div id="banner-text">
   		<h1><?php the_field('lander_header_title'); ?></h1>
   		<h2><?php the_field('lander_header_sub_title'); ?></h2>
      <p><?php the_field('lander_header_description'); ?></p>
   	</div>

    <?php if( have_rows('lander_header_call_to_action_details') ):

     while( have_rows('lander_header_call_to_action_details') ): the_row();

       // vars
       $cta_title = get_sub_field('lander_header_call_to_action_title');

       ?>

       <div id="banner-content">

         <div class="checks">

           <h3><?php echo $cta_title; ?></h3>

           <ul>

             <?php while( have_rows('lander_header_call_to_action_check_items') ): the_row();

               // vars
               $cta_item_title = get_sub_field('lander_header_call_to_action_check_item_title');
               $cta_item_description = get_sub_field('lander_header_call_to_action_check_item_description');
               ?>

               <li class="item">
                 <div>
                   <h4><?php echo $cta_item_title; ?></h4>
                   <p><?php echo $cta_item_description; ?></p>
                 </div>
               </li>

              <?php endwhile; ?>

            </ul>

         </div>

         <?php endwhile; ?>

          <?php endif; ?>

          <?php

          $post_object = get_field('lander_header_cta_form');

          if( $post_object ):

            // override $post
            $post = $post_object;
            setup_postdata( $post );

            ?>
              <?php echo do_shortcode( '[wpforms id="'.get_field('lander_header_cta_form').'" title="true" description="true" ]' ); ?>
              <?php wp_reset_postdata(); ?>
          <?php endif; ?>

       </div>

  </div>

  <div id="banner-circles">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>

 </div>
