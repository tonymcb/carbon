<?php
/**
 * Block Name: About stats
 *
 * This is the template that displays the testimonial block.
 */

// get image field (array)
$avatar = get_field('avatar');

// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';

?>
<div id="about-stats">
  <div id="stat-wrapper">
    <div id="stats">
      <div class="stat-item">
        <div class="line">
          <object type="image/svg+xml" data="<?php bloginfo('template_url'); ?>/img/line_blue.svg"></object>
        </div>
        <h2><strong><?php the_field('stat_one'); ?></strong></h2>
        <h4><?php the_field('stat_one_description'); ?></h4>
      </div>
      <div class="stat-item">
        <div class="line">
          <object type="image/svg+xml" data="<?php bloginfo('template_url'); ?>/img/line_orange.svg"></object>
        </div>
        <h2><strong><?php the_field('stat_two'); ?></strong></h2>
        <h4><?php the_field('stat_two_description'); ?></h4>
      </div>
      <div class="stat-item">
        <div class="line">
          <object type="image/svg+xml" data="<?php bloginfo('template_url'); ?>/img/line_green.svg"></object>
        </div>
        <h2><strong><?php the_field('stat_three'); ?></strong></h2>
        <h4><?php the_field('stat_three_description'); ?></h4>
      </div>
    </div>
    <div id="stat-image">
      <?php

      $image = get_field('stat_image');

      if( !empty($image) ): ?>

      	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" />

      <?php endif; ?>
    </div>
  </div>
</div>
