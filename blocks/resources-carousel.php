<?php
/**
 * Block Name: Resources carousel
 *
 * This is the template that displays the resources carousel.
 */

 // create id attribute for specific styling
 $id = 'resources-carousel-' . $block['id'];

 ?>

 <?php
 // the query
 $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'category__not_in' => array(12),'posts_per_page'=>-1)); ?>

 <?php if ( $wpb_all_query->have_posts() ) : ?>


 <ul id="posts" class="carousel">

   <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
       <li>
         <div>

           <header>
               <div class="tags"><?php the_tags( ' ', '' ); ?></div>
               <?php if ( has_post_thumbnail() ) : ?>
               <div><a href="<?php the_permalink(); ?>" class="link"><?php the_post_thumbnail( array(400, 300) ); ?></a></div>
               <?php endif; ?>
           </header>

           <div class="post-content">
             <h4><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h4>
             <div class="author">
               <?php echo get_avatar( get_the_author_meta( 'ID' ) ); ?>
               <?php the_author_posts_link(); ?>
             </div>
             <div class="meta">
              <span><?php the_date(); ?></span>
            </div>
             <div class="entry"><?php echo get_the_excerpt(); ?></div>


           </div>

           <footer>
             <span class="wp-block-button is-style-arrow link is-style-arrow-link"><a href="<?php the_permalink(); ?>" class="wp-block-button__link">Read more</a></span>
           </footer>

         </div>

       </li>
   <?php endwhile; ?>

 </ul>

 <?php wp_reset_postdata(); ?>

 <?php endif; ?>
