<?php
/**
 * Block Name: Timeline
 *
 * This is the template that displays the timeline block.
 */

// get image field (array)
$avatar = get_field('avatar');

// create id attribute for specific styling
$id = 'timeline-' . $block['id'];

?>


<?php if( have_rows('timeline_item') ): ?>

	<div id="<?php echo $id; ?>" class="timeline">

		<?php the_field('timeline_intro'); ?>

    <ul>

  	<?php while( have_rows('timeline_item') ): the_row();

  		// vars
  		$icon = get_sub_field('icon');
  		$title = get_sub_field('title');
  		$text = get_sub_field('text');

  		?>

  		<li class="timeline-item">
				<div>
	        <div class="icon"><img src="<?php echo $icon['url']; ?>" alt="<?php echo $icon['alt'] ?>" /></div>
	        <h4><?php echo $title; ?></h4>
	        <span><?php echo $text; ?></span>
				</div>
  		</li>

  	<?php endwhile; ?>

    </ul>

  </div>

<?php endif; ?>
