<?php
/**
 * Block Name: Banner
 *
 * This is the template that displays the contact information block.
 */

// get image field (array)
$avatar = get_field('avatar');

?>

<div id="banner" class="<?php if( get_field('banner_image_toggle') ): ?>banner-image<?php endif; ?>">

	<div id="banner-text">
		<h1><?php the_field('title'); ?></h1>
		<h2><?php the_field('description'); ?></h2>
	</div>

</div>

<?php

$image = get_field('banner_feature_image');

if( !empty($image) ): ?>

<div id="banner-feature-image">
	<img src="<?php echo $image['url']; ?>" alt="<?php echo $image['alt']; ?>" class="banner-image"/>
</div>

<?php endif; ?>
