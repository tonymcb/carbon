<?php
/**
 * Block Name: Stats
 *
 * This is the template that displays the stats block.
 */


// create align class ("alignwide") from block setting ("wide")
$align_class = $block['align'] ? 'align' . $block['align'] : '';
$align = get_field('align');
?>

<div class="stats align-<?php echo $align['value']; ?>">

<?php while( have_rows('stats') ): the_row();?>

  <div class="stat">

    <h2><?php the_sub_field('stat'); ?></h2>
    <h5><?php the_sub_field('stat_description'); ?></h5>

  </div>

<?php endwhile; ?>
</div>
