wp.domReady( () => {
	wp.blocks.unregisterBlockStyle( 'core/button', 'default' );
	wp.blocks.unregisterBlockStyle( 'core/button', 'squared' );

	wp.blocks.registerBlockStyle( 'core/button', {
		name: 'arrow-link',
		label: 'Arrow Link',
		isDefault: true,
	});

	wp.blocks.registerBlockStyle( 'core/button', {
		name: 'green',
		label: 'green',
		isDefault: true,
	});

} );
