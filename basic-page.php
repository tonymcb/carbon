<?php

/*
* Template Name: Basic page template
* Template Post Type: page
*/

get_header();


?>

<div id="container">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>

<?php the_content(); ?>

<div class="entry-links"><?php wp_link_pages(); ?></div>


<?php if ( comments_open() && ! post_password_required() ) { comments_template( '', true ); } ?>
<?php endwhile; endif; ?>

<?php get_footer(); ?>
