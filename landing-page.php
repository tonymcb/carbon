<?php

/*
* Template Name: Landing page template
* Template Post Type: page
*/

get_header('lander');


?>

<div id="container">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>


<?php the_content(); ?>

<?php the_field('heading'); ?>

<?php endwhile; endif; ?>


<?php get_footer('lander'); ?>
