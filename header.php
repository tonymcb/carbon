<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<?php
function slick() {
    wp_register_script('slick', get_template_directory_uri() . '/js/slick.js', true );
    wp_enqueue_script('slick');
}

add_action( 'wp_enqueue_scripts', 'slick' );
?>
<?php
function custom() {
    wp_register_script('custom', get_template_directory_uri() . '/js/custom.js', true );
    wp_enqueue_script('custom');
}

add_action( 'wp_enqueue_scripts', 'custom' );
?>

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-KWSGHDJ');</script>
<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
    <script>
    !function(f,b,e,v,n,t,s)
    {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
    n.callMethod.apply(n,arguments):n.queue.push(arguments)};
    if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
    n.queue=[];t=b.createElement(e);t.async=!0;
    t.src=v;s=b.getElementsByTagName(e)[0];
    s.parentNode.insertBefore(t,s)}(window, document,'script',
    'https://connect.facebook.net/en_US/fbevents.js');
    fbq('init', '1461368644029507');
    fbq('track', 'PageView');
    </script>
    <noscript><img height="1" width="1" style="display:none"
    src="https://www.facebook.com/tr?id=1461368644029507&ev=PageView&noscript=1"
    /></noscript>
    <!-- End Facebook Pixel Code -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
<script type="text/javascript" async defer src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-KWSGHDJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<!-- Quantcast Choice. Consent Manager Tag -->
<script type="text/javascript" async=true>
  var elem = document.createElement('script');
  elem.src = 'https://quantcast.mgr.consensu.org/cmp.js';
  elem.async = true;
  elem.type = "text/javascript";
  var scpt = document.getElementsByTagName('script')[0];
  scpt.parentNode.insertBefore(elem, scpt);
  (function () {
    var gdprAppliesGlobally = false;
    function addFrame() {
      if (!window.frames['__cmpLocator']) {
        if (document.body) {
          var body = document.body,
            iframe = document.createElement('iframe');
          iframe.style = 'display:none';
          iframe.name = '__cmpLocator';
          body.appendChild(iframe);
        } else {
          // In the case where this stub is located in the head,
          // this allows us to inject the iframe more quickly than
          // relying on DOMContentLoaded or other events.
          setTimeout(addFrame, 5);
        }
      }
    }
    addFrame();
    function cmpMsgHandler(event) {
      var msgIsString = typeof event.data === "string";
      var json;
      if (msgIsString) {
        json = event.data.indexOf("__cmpCall") != -1 ? JSON.parse(event.data) : {};
      } else {
        json = event.data;
      }
      if (json.__cmpCall) {
        var i = json.__cmpCall;
        window.__cmp(i.command, i.parameter, function (retValue, success) {
          var returnMsg = {
            "__cmpReturn": {
              "returnValue": retValue,
              "success": success,
              "callId": i.callId
            }
          };
          event.source.postMessage(msgIsString ?
            JSON.stringify(returnMsg) : returnMsg, '*');
        });
      }
    }
    window.__cmp = function (c) {
      var b = arguments;
      if (!b.length) {
        return __cmp.a;
      }
      else if (b[0] === 'ping') {
        b[2]({
          "gdprAppliesGlobally": gdprAppliesGlobally,
          "cmpLoaded": false
        }, true);
      } else if (c == '__cmp')
        return false;
      else {
        if (typeof __cmp.a === 'undefined') {
          __cmp.a = [];
        }
        __cmp.a.push([].slice.apply(b));
      }
    }
    window.__cmp.gdprAppliesGlobally = gdprAppliesGlobally;
    window.__cmp.msgHandler = cmpMsgHandler;
    if (window.addEventListener) {
      window.addEventListener('message', cmpMsgHandler, false);
    }
    else {
      window.attachEvent('onmessage', cmpMsgHandler);
    }
  })();
  window.__cmp('init', {
    'Language': 'en',
    'Initial Screen Reject Button Text': 'I do not accept',
    'Initial Screen Accept Button Text': 'I accept',
    'Initial Screen Purpose Link Text': 'View and change consent preferences',
    'Purpose Screen Body Text': 'You can set your consent preferences and determine how you want your data to be used based on the purposes below. You may set your preferences for us independently from those of third-party partners. Each purpose has a description so that you know how we and partners use your data.',
    'Vendor Screen Body Text': 'You can set consent preferences for each individual third-party company below. Expand each company list item to see what purposes they use data for to help make your choices. In some cases, companies may disclose that they use your data without asking for your consent, based on their legitimate interests. You can click on their privacy policies for more information and to opt out.',
    'Vendor Screen Accept All Button Text': 'Accept all',
    'Vendor Screen Reject All Button Text': 'Reject all',
    'Initial Screen Body Text': '<p class="smalltext">We and our partners may use cookies and similar technologies to collect user data for the following purposes:</p>\n\n<ul class="smalltext"><li><strong>Required cookies</strong> - necessary for the site to function</li>\n\n<li><strong>Functional cookies</strong> - help us to optimize the site</li>\n\n<li><strong>Advertising cookies</strong> - allow us and <a id="minilink" class="qc-cmp-alt-action" onclick="window.__cmpui('+"'"+"updateConsentUi"+ "'" +',2)">our partners</a> to provide you with personalised advertising and tailored content across your browsers and devices</li></ul>\n\n<p class="smalltext">Click below to consent. Please visit our <a href="/privacy.html" target="_blank">privacy policy</a> for more information.</p>',
    // 'Initial Screen Body Text Option': 1,
    'Publisher Name': 'Clicksco Digital',
    'Publisher Logo': 'https://docs.carbondmp.com/static/carbon-by-clicksco-ba9daa4521ea4c120c3b77dd13a4ed8c.png',
    'Publisher Purpose IDs': [1, 2, 3, 4, 5],
    'Publisher Purpose Legitimate Interest IDs': [1, 5],
    'Post Consent Page': 'https://carbondmp.com/',
    'Consent Scope': 'service',
    'Min Days Between UI Displays': 366,
  });
</script>
<!-- End Quantcast Choice. Consent Manager Tag -->
<div id="wrapper" class="hfeed">

<header id="header">

  <div id="contents">

    <div id="logo"><a href="<?php echo site_url(); ?>"><img src="<?php bloginfo('template_url'); ?>/img/logo.svg" alt="Carbon logo"></a></div>
    <nav id="menu">
      <div id="nav-icon">
        <div></div>
      </div>
    <?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>

    </nav>

  </div>

</header>

<div id="circles">
  <span></span>
  <span></span>
  <span></span>
  <span></span>
  <span></span>
</div>
