</div>
<footer id="footer">
  <div id="newsletter">
    <div>
      <h3>Newsletter Sign Up</h3>
      <span>Sign up to our newsletter to learn about the latest product updates and resources</span>

      <!-- Begin Mailchimp Signup Form -->
      <div id="mc_embed_signup">

        <form action="https://carbondmp.us9.list-manage.com/subscribe/post?u=c3f0a685a85eb65b3affdc5d6&amp;id=45ad928528" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>

            <div id="mc_embed_signup_scroll">

              <div class="mc-field-group">
              	<label for="mce-EMAIL">Your email</label>
              	<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL">
              </div>
            	<div id="mce-responses" class="clear">
            		<div class="response" id="mce-error-response" style="display:none"></div>
            		<div class="response" id="mce-success-response" style="display:none"></div>
            	</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
              <div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_c3f0a685a85eb65b3affdc5d6_45ad928528" tabindex="-1" value=""></div>
              <div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button"></div>

            </div>

        </form>

      </div>
      <script type='text/javascript' src='//s3.amazonaws.com/downloads.mailchimp.com/js/mc-validate.js'></script><script type='text/javascript'>(function($) {window.fnames = new Array(); window.ftypes = new Array();fnames[0]='EMAIL';ftypes[0]='email';fnames[1]='MMERGE1';ftypes[1]='text';fnames[2]='MMERGE2';ftypes[2]='text';fnames[3]='MMERGE3';ftypes[3]='text';}(jQuery));var $mcj = jQuery.noConflict(true);</script>
      <!--End mc_embed_signup-->

    </div>
  </div>
  <div id="footer-content">
    <div id="footer-logo"><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/logo-white.svg" alt="Carbon logo"></a></div>
    <?php wp_nav_menu( array( 'theme_location' => 'footer-menu' ) ); ?>
  </div>
</footer>
<div id="copyright">
  <span>Copyright &copy; <?php echo date("Y"); ?> Carbon (AI) Ltd</span>
</div>
</div>
<?php wp_footer(); ?>
<script>

    window._ccReady = window._ccReady || [];
    window._ccReady.push(settings => {

        window.cca.api.audiencesReady(function (audiences) {

            fbq('trackCustom', 'CarbonSegmentEvent', {
                carbonSegement: audiences
            });

        });

    });

</script>
</body>
</html>
