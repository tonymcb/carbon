</div>
<footer id="footer" class="footer-lander">
  <div id="footer-content">
    <div>
      <h3><?php the_field('lander_footer_title'); ?></h3>
      <span><?php the_field('lander_footer_text'); ?></span>
      <div id="cta-form">
        <?php echo do_shortcode( '[wpforms id="'.get_field('lander_footer_form').'"]' ); ?>
      </div>
    </div>
  </div>
</footer>
<div id="copyright">
  <div>
    <div id="footer-logo"><a href="#"><img src="<?php bloginfo('template_url'); ?>/img/logo-white.svg" alt="Carbon logo"></a></div>
    <span>Copyright &copy; <?php echo date("Y"); ?> Clicksco UK Ltd</span>
  </div>
</div>
</div>
<?php wp_footer(); ?>
</body>
</html>
