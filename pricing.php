<?php

/*
* Template Name: Pricing
* Template Post Type: page
*/

get_header();


?>

<div id="container">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

  <?php endwhile; endif; ?>

<div class="comparison">

  <table>
    <thead>
      <tr>
        <th class="tl tl2" width="250"></th>
        <th class="qbse">
          Carbon light
        </th>
        <th colspan="3" class="qbo">
          Carbon
        </th>
      </tr>
      <tr>
        <th class="tl"></th>
        <th class="compare-heading">

        </th>
        <th class="compare-heading">
          Standard
        </th>
        <th class="compare-heading">
          Advanced Insights
        </th>
        <th class="compare-heading">
          Enterprise
        </th>
      </tr>

    </thead>
    <tbody>


      <?php if( have_rows('pricing_entry') ): ?>

        	<?php while( have_rows('pricing_entry') ): the_row();

        		// vars
        		$title = get_sub_field('title');
        		$light = get_sub_field('light');
        		$standard = get_sub_field('standard');
            $advanced = get_sub_field('advanced_insights');
            $enterprise = get_sub_field('enterprise');
            $hastitle = get_sub_field('is_section_title');

        		?>

            <?php if( $hastitle ): ?><tr class="heading"><?php else: ?><tr><?php endif; ?>
              <td></td>
              <td colspan="4" class="no-check"><?php echo $title; ?></td>
            </tr>


            <?php if( $hastitle ): ?><tr class="compare-row heading"><?php else: ?><tr class="compare-row"><?php endif; ?>
              <?php if( $hastitle ): ?><td class="section-title"><?php echo $title; ?></td><?php else: ?><td><?php echo $title; ?></td><?php endif; ?>
              <?php if( $light ): ?><td class="data"><?php echo $light; ?></td><?php else: ?><td></td><?php endif; ?>
              <?php if( $standard ): ?><td class="data"><?php echo $standard; ?></td><?php else: ?><td></td><?php endif; ?>
              <?php if( $advanced ): ?><td class="data"><?php echo $advanced; ?></td><?php else: ?><td></td><?php endif; ?>
              <?php if( $enterprise ): ?><td class="data"><?php echo $enterprise; ?></td><?php else: ?><td></td><?php endif; ?>
            </tr>

        	<?php endwhile; ?>

          <tfoot>
            <tr>
              <td>Cost</td>
              <td><?php the_field('light_total'); ?></td>
              <td><?php the_field('standard_total'); ?></td>
              <td><?php the_field('advanced_insights_total'); ?></td>
              <td><?php the_field('enterprise_total'); ?></td>
            </tr>
          </tfoot>


      <?php endif; ?>
    </tbody>
  </table>

</div>

<?php get_footer();?>
