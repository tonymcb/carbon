<?php

/*
* Template Name: Contact us
* Template Post Type: page
*/

get_header();


?>

<div id="container">

<?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

<?php if ( has_post_thumbnail() ) { the_post_thumbnail(); } ?>

<?php the_content(); ?>

<?php echo do_shortcode( '[wpforms id="394" title="false" description="false"]' ); ?>


<div id="map">
	<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d37041.26922014671!2d-1.1974289805694962!3d54.53209665733224!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x487eed0e8878250b%3A0xae7f9db6bd8af701!2sClicksco!5e0!3m2!1sen!2suk!4v1555320410859!5m2!1sen!2suk" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
</div>

<?php endwhile; endif; ?>

<?php get_footer();?>
