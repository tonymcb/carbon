<?php

/*
* Template Name: Careers
* Template Post Type: page
*/

get_header();


?>

<div id="container">

  <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>

  <?php the_content(); ?>

  <?php endwhile; endif; ?>

  <?php
  // the query
  $wpb_all_query = new WP_Query(array('post_type'=>'post', 'post_status'=>'publish', 'cat' => array(12),'posts_per_page'=>-1)); ?>

  <?php if ( $wpb_all_query->have_posts() ) : ?>


  <ul id="posts">

    <?php while ( $wpb_all_query->have_posts() ) : $wpb_all_query->the_post(); ?>
        <li>
          <div>
            <div class="post-content">
              <h4><?php the_title(); ?></h4>
              <div class="author">

              </div>
              <div class="entry">
                 <?php the_excerpt(); ?>
               </div>
            </div>
            <footer>
              <span class="wp-block-button is-style-arrow link is-style-arrow-link"><a href="<?php the_permalink(); ?>" class="wp-block-button__link">Read more</a></span>
            </footer>
          </div>

        </li>
    <?php endwhile; ?>

  </ul>

  <?php wp_reset_postdata(); ?>

  <?php endif; ?>


<?php get_footer();?>
